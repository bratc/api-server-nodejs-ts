import { Request, Response } from 'express';
import BookModel, { Book } from './BookModel';

export default class BooksController{
    public constructor(){}

    public async index(req: Request, res: Response){
        const books = await BookModel.find();
        return res.json(books);
    }

    public async save(req: Request, res: Response){
        const { title, author, isbn } = req.body;
        const book: Book =  new BookModel({ title, author, isbn })
        const bookRes =  await book.save();
        res.send(bookRes);
    }
}