import { Router } from 'express';
import BooksController from './BooksController';
const router: Router = Router();

const booksController = new BooksController();

router.get('/books', booksController.index);
router.post('/books', booksController.save);

export default router;