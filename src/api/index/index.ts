import { Router } from 'express';
import IndexController from './IndexController';
const router: Router = Router();

const indexsController = new IndexController();

router.get('/index', indexsController.index);

export default router;