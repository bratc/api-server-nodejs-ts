import express from 'express';
import exphbs from 'express-handlebars';
import path from 'path';
import BookRouter from './api/books';
import IndexRouter from './api/index';
//Initializatins
const app = express();

import './database';

//Config Server
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.engine('.hbs', exphbs({
    extname: ".hbs",
    layoutsDir: path.join(app.get('views'), 'layouts'),
    partialsDir: path.join(app.get('views'), 'partials'),
    helpers: require('./lib/helpers')
}));
app.set('views engine','.hbs');
//Middlewares

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

//Routes

app.use('/api/',BookRouter);
app.use('/api/',IndexRouter);

//Static Files
app.use(express.static(path.join(__dirname,'public')));

// Starting the server

app.listen(app.get('port'), () => {
    console.log(`Server Staring in http://localhost:${app.get('port')} `)
});